import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    routes: {
      ExtractArgumentsScreen.routeName: (context) => const ExtractArgumentsScreen(),
    },
    home: HomeScreen(),
  );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pushNamed(
              context, ExtractArgumentsScreen.routeName, 
              arguments: ScreenArgument('Nutthaporn Title', 
              'Nutthaporn message'
            ));
          },
          child: Text('Navigate to screen that ectracts arguments'),
        ),
      ),
    );
  }
}

class ScreenArgument{
  final String title;
  final String message;

  ScreenArgument(this.title, this.message);
}

class ExtractArgumentsScreen extends StatelessWidget {
  const ExtractArgumentsScreen({Key? key}) : super(key: key);

  static const routeName = '/extractArguments';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArgument;
    return Scaffold(
      appBar: AppBar(
        title: Text(args.title),
      ),
      body: Center(
        child: Text(args.message),
      ),
    );
  }
}